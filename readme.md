# Forcebrands App 

### Getting Started
This is a Laravel Project for Web Application fo Forcebrands.com

### Requirements

PHP >= 5.6
NodeJS >= 4*

### Installation

```

$ composer install
$ npm install
$ cp .env.example .env
$ php artisan key:generate

```

### Running locally
You can follow one of these options
- 1. Run via artisan `$ php artisan serve` to http://127.0.0.1:8000
- 2. Make a virtual host

### Making virtual host

#### Nginx (pending)

#### Using Apache 2 (pending)

by SneakersAgency.com